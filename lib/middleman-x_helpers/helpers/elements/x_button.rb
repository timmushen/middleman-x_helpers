########################################################
#
# Button
#
########################################################
def x_button(url: "", icon: "", color: "primary", target: "_self", css: "", text: "")
	
	unless icon == ""
		icon = "<i class='fa #{icon}'></i> "
	else
		icon = ""
	end
	
	"<a href='#{url}' target='#{target}' class='#{css} btn-#{color}'>#{icon}#{text}</a>"
end