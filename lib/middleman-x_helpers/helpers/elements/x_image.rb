########################################################
#
# x Image
#
########################################################
def x_image(
	src: "http://www.fillmurray.com/360/480", 
	width: "280", 
	height: "360",
	corners: "none", #circle, rounded, thumbnail
	link: "",
	link_target: "",
	responsive: true, 
	css: "",
	alt: ""
	)
	

	# corners
	if corners == "rounded"
		css = css + " img-rounded"
	elsif corners == "circle"
		css = css + " img-circle"
	elsif corners == "thumbnail"
		css = css + " img-thumbnail"
	end

	# responsive 
	if responsive == true
		css = css + " img-responsive"
	end

	template = File.read(File.expand_path('../views/x_image.erb', __FILE__))
	ERB.new(template).result(binding) 
end