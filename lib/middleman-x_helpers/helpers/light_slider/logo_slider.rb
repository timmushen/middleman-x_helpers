########################################################
#
# X Logo Slider
#
########################################################
def x_logo_slider(id: "", slides: array)

@slide_builder = ""
slides.each do |el|
n = ""
n =<<EOS
<li class="x_slide-item">
	<img src="#{el[:img]}" alt="">
</li>
EOS
@slide_builder  << n
end

	template = File.read(File.expand_path('../views/x_slider.erb', __FILE__))
	ERB.new(template).result(binding) 
end