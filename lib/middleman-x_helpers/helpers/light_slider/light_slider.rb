########################################################
#
# X Light Slider
#
########################################################
def x_slider(id: "", slides: array)

@slide_builder = ""
slides.each do |el|
n = ""
n =<<EOS
<li class="x_slide-item">
	<h3>#{el[:h3]}</h3>
	<p>#{el[:p]}</p>
</li>
EOS
@slide_builder  << n
end

	template = File.read(File.expand_path('../views/x_slider.erb', __FILE__))
	ERB.new(template).result(binding) 
end