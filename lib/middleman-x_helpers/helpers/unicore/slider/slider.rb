########################################################
#
# Slider
#
# slides: [
# 		{background_image: "/images/image.jpg", text: "Slide", align: "left", subtext: "Develop", button_text: "", button_icon: "", button_css: "", button_url: ""},
# 	
# 		]),
#
########################################################
def unicore_slider(
		interval: 5000, 
		background_color: "#FFFFFF",
		slides: array, 
		id: "slider-0",
		after_navbar: false
	)


################# Instace vars #################
@interval = interval
@background_color = background_color
@slides = slides
@id = id

################# After Navbar #################
if after_navbar == true
	@after_navbar = "mbr-after-navbar"
else 
	@after_navbar = ""
end

################# type #################
	template = File.read(File.expand_path('../views/slider.erb', __FILE__))
	ERB.new(template).result(binding) 
end