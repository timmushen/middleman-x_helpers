########################################################
#
# Header
#
########################################################
def unicore_header(
		type: "4", 
		background_image: "http://electricblaze.com/unicore/page-builder/assets/images/11.jpg", 
		title: "", 
		body: "", 
		after_navbar: false, 
		cta: "",
		overlay_color: "rgb(21, 21, 21)",
		overlay_opacity: "0.75",
		cta_class: "btn-lg btn-primary", 
		cta_icon: "fa-check", 
		cta_image: "http://electricblaze.com/unicore/page-builder/assets/images/watch.png"
	)


################# Instace vars #################
@title = title
@body = body
@background_image = background_image
@cta = cta
@cta_class = cta_class
@cta_icon = cta_icon
@cta_image = cta_image
@overlay_opacity = overlay_opacity
@overlay_color = overlay_color

################# After Navbar #################
if after_navbar == true
	@after_navbar = "mbr-after-navbar"
else 
	@after_navbar = ""
end

################# type #################
if type == "4"
	template = File.read(File.expand_path('../views/header_4.erb', __FILE__))
elsif type == "8"
	template = File.read(File.expand_path('../views/header_8.erb', __FILE__))
elsif type == "18"
	template = File.read(File.expand_path('../views/header_18.erb', __FILE__))
elsif type == "21"
	template = File.read(File.expand_path('../views/header_21.erb', __FILE__))
else
	template = File.read(File.expand_path('../views/header_4.erb', __FILE__))
end

	ERB.new(template).result(binding) 
end