########################################################
#
# Message
#
########################################################
def unicore_message(
		type: "4", 
		background_color: "rgb(41, 217, 194)", 
		title: "", 
		body: "", 
		after_navbar: false, 
		cta: "",
		cta_link: "",
		cta_class: "btn-default",
		cta_image: ""
	)


################# Instace vars #################
@title = title
@body = body
@background_color = background_color
@cta = cta
@cta_link = cta_link
@cta_class = cta_class
@cta_image = cta_image

################# After Navbar #################
if after_navbar == true
	@after_navbar = "mbr-after-navbar"
else 
	@after_navbar = ""
end

################# type #################
if type == "9"
	template = File.read(File.expand_path('../views/message_9.erb', __FILE__))
elsif type == "1"
	template = File.read(File.expand_path('../views/message_1.erb', __FILE__))
elsif type == "8"
	template = File.read(File.expand_path('../views/message_8.erb', __FILE__))
# elsif type == "21"
# 	template = File.read(File.expand_path('../views/header_21.erb', __FILE__))
else
	template = File.read(File.expand_path('../views/message_9.erb', __FILE__))
end
	ERB.new(template).result(binding) 
end