########################################################
#
# features
#
# feature: [
# 		{feature_conf: "", title: "Slide", body: "Lorem ipsum dolor sit amet elit convallis"},
# 		]),
#
########################################################
def unicore_features(
		background_color: "#FFFFFF",
		features: array
	)


################# Instace vars #################
@background_color = background_color
@features = features

################# type #################
	template = File.read(File.expand_path('../views/features.erb', __FILE__))
	ERB.new(template).result(binding) 
end