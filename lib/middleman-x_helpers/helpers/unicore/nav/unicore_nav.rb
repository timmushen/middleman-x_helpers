########################################################
#
# Unicore Nav
# <%= unicore_nav(
# 	logo: "http://loremlabs.co/graphics/LoremLabs_logo.png",
# 	logo_alt: "Logo",
# 	nav: [
# 		{name: "About", css: "nav-item", link: "#"},
# 		{name: "Services", css: "nav-item", link: "#"},
# 		]),
# 	buttons: [
# 		{name: "About", css: "nav-item", link: "#", icon: "fa-check"},
# 	]
# %>
#
########################################################
def unicore_nav(logo: "", logo_alt: "", brand: "", nav: array, button: array, collapsed: false, sticky: false)


################# Collapesd Nav #################

@collapsed = "mbr-navbar--auto-collapse"
if collapsed == true  
	@collapsed = "mbr-navbar--collapsed"
end

################# Sticky Nav #################

@sticky = ""
if sticky == true  
	@sticky = "mbr-navbar--sticky"
end


################# Logo #################
@logo = logo


################# Brand #################
@brand = brand

################# Logo Alt #################

@logo_alt = logo_alt



################# Build Nav #################

@nav_builder = ""
nav.each do |el|
n = ""
n =<<EOS
<a class="text-white nav-link link #{el[:css]}" href="#{el[:link]}">#{el[:name]}</a>
EOS
@nav_builder  << n
end

################# Button Nav #################

@button_builder = ""
button.each do |el|
b = ""
b =<<EOS
<a class="nav-link btn btn-primary nav-item simplebutton #{el[:css]}" href="#{el[:link]}"><span class="fa #{el[:icon]}"></span> #{el[:name]}</a>
EOS
@button_builder  << b
end

	template = File.read(File.expand_path('../views/unicore_nav.erb', __FILE__))
	ERB.new(template).result(binding) 
end