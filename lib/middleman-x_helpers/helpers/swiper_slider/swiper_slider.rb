########################################################
#
# Swiper Slider
#
########################################################
def x_swiper_slider(id: "", slides: array)
@id = id
@slide_builder = ""
slides.each do |el|
n = ""
n =<<EOS
<div class="swiper-slide">Slide 1</div>
<li>
  <img src="#{el[:img]}" alt="">
  <div class="container">
    <h3>#{el[:h3]}</h3>
		<p>#{el[:p]}</p>
  </div>
</li>
EOS
@slide_builder  << n
end

	template = File.read(File.expand_path('../views/x_swiper_slider.erb', __FILE__))
	ERB.new(template).result(binding) 
end