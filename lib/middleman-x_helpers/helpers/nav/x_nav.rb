########################################################
#
# X Nav
#
########################################################
def x_nav(logo: "", logo_alt: "", fluid: false, nav: array)
@fluid = ""
if fluid == true  
	@fluid = "-fluid"
end
@nav_builder = ""
nav.each do |el|
n = ""
n =<<EOS
<li class="x_nav-item">
	<a class="#{el[:css]}" href="#{el[:link]}" class="x_nav-item-a">#{el[:name]}</a>
</li>
EOS
@nav_builder  << n
end

	template = File.read(File.expand_path('../views/x_nav.erb', __FILE__))
	ERB.new(template).result(binding) 
end