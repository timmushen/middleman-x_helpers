################# AMP Video #################
def x_amp_video(width:"400",height:"300",src:"",poster:"",fallback_msg:"Your browser doesn’t support HTML5 video",mp4:"",webm:"")

unless mp4 == ""
	mp4 = "<source type='video/mp4' src='#{mp4}'>"
end

unless webm == ""
	webm = "<source type='video/webm' src='#{webm}'>"
end

video =<<EOS
<amp-video width=400 height=300 src="https://yourhost.com/videos/myvideo.mp4"
    poster="myvideo-poster.jpg">
  <div fallback>
    <p>Your browser doesn’t support HTML5 video</p>
  </div>
 #{mp4}
 #{webm}
</amp-video>
EOS
video
end