################# AMP Image #################
def x_amp_img(src:"",width:"",height:"",layout:"responsive",alt:"")
img =<<EOS
<amp-img src="#{src}"
width="#{width}"
height="#{height}"
layout="#{layout}"
alt="#{alt}"></amp-img>
EOS
img
end