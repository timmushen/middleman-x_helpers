########################################################
#
# SuperSlider
#
########################################################
def x_superslider(id: "", slides: array)
@id = id
@slide_builder = ""
slides.each do |el|
n = ""
n =<<EOS
<li>
  <img src="#{el[:img]}" alt="">
  <div class="container">
    <h3>#{el[:h3]}</h3>
		<p>#{el[:p]}</p>
  </div>
</li>
EOS
@slide_builder  << n
end

	template = File.read(File.expand_path('../views/x_superslider.erb', __FILE__))
	ERB.new(template).result(binding) 
end