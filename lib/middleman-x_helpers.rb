# require Middleman Core
require "middleman-core"

# x helper class for config.rb 
class XHelpers < Middleman::Extension
  
	# init method | leaving room for block configuration options at a later point
  def initialize(app, options_hash={}, &block)
    super
  end

  # include all of the *.rb files in the helpers foler
  helpers do
	  Dir[File.join(File.dirname(__FILE__), '..',  "lib/middleman-x_helpers/helpers", '**/*.rb')].sort.each do |file|
		  require file
		end
  end
end


# register helpers in Middleman
::Middleman::Extensions.register(:x_helpers, XHelpers)