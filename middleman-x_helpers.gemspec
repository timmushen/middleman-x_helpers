# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)

Gem::Specification.new do |s|
  s.name        = "middleman-x_helpers"
  s.version     = "0.0.2"
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Tim Mushen"]
  s.email       = ["tim.mushen@gmail.com"]
  s.homepage    = "https://bitbucket.org/timmushen/middleman-x_helpers"
  s.summary     = %q{Helpers for Middleman}
  s.description = %q{Helpers for Middleman}

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
  
  # The version of middleman-core your extension depends on
  s.add_runtime_dependency("middleman-core")
  
  # Additional dependencies
  # s.add_runtime_dependency("gem-name", "gem-version")
end
